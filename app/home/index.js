import React, { useState } from "react";
import { LinearGradient } from "expo-linear-gradient";
import {
  ScrollView,
  Image,
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Platform,
  Alert,
  Modal,
  Pressable,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import MapboxGL from "@react-native-mapbox-gl/maps";
import { getBicloo } from "../../services/JCDecaux";
import ListBicloo from "./ListBicloo";

MapboxGL.setAccessToken(
  "pk.eyJ1IjoibXJub2JvZHlzIiwiYSI6ImNrdDAybGhjYTA4cXEyb283NWk4MzduZGQifQ.BjMsS3B5DszDUIzI_a2IUw"
);

class HomeView extends React.Component {
  state = {
    modalVisible: false,
    dataBicloo: [],
  };
  async componentDidMount() {
    const dataBicloo = await getBicloo();

    this.setState({
      dataBicloo,
    });
  }

  static navigationOptions = ({ navigation }) => ({
    headerTitle: () =>
      Platform.OS === "ios" ? (
        <Image
          source={require("../../assets/images/bicloo.png")}
          style={{ width: 120, height: 30 }}
        />
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Image
            source={require("../../assets/images/bicloo.png")}
            style={{
              width: 120,
              height: 30,
            }}
          />
        </View>
      ),
    headerBackground: () => (
      <LinearGradient
        colors={["#f4fefe", "#f4fefe", "#9E9B9A"]}
        style={{ flex: 1 }}
      />
    ),
  });

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  };
  render() {
    const { navigate } = this.props.navigation;
    const { modalVisible, dataBicloo } = this.state;
    return (
      <MapboxGL.MapView style={styles.map}>
        <View>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              this.setModalVisible(!modalVisible);
            }}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <Text style={styles.modalText}>
                  {dataBicloo &&
                    dataBicloo.length > 0 &&
                    dataBicloo.map((bicloo, index) => {
                      return (
                        <View key={`${index}-dataBicloo`}>
                          <ListBicloo bicloo={bicloo} />
                        </View>
                      );
                    })}
                </Text>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => this.setModalVisible(!modalVisible)}
                >
                  <Text style={styles.textStyle}>Hide Modal</Text>
                </Pressable>
              </View>
            </View>
          </Modal>
          <Pressable
            style={[styles.button, styles.buttonOpen]}
            onPress={() => this.setModalVisible(true)}
          >
            <Text style={styles.IonIcons}>
              <Ionicons
                name={Platform.OS === "ios" ? "ios-arrow-up" : "md-arrow-up"}
                color="white"
                size={25}
              />
            </Text>
          </Pressable>
        </View>
      </MapboxGL.MapView>
    );
  }
}

const Home = connect()(HomeView);
export { Home };

const styles = {
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  map: {
    flex: 1,
  },
  IonIcons: {},

  modalView: {
    flex: 2,
    marginTop: 100,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 150,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    marginHorizontal: 185,
    top: 720,
    backgroundColor: "#000",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
};

// import React, { useEffect } from "react";
// import { View, Text } from "react-native";
// import { connect } from "react-redux";
// import { GET_ALL_USER_INFO_REQUEST } from "../models/data/actions";
// import { Maps } from "./components/map";

// const mapStateToProps = (state, props) => {
//   const { id, name } = state.user;
//   return { id, name };
// };
// const mapDispatchToProps = (dispatch, props) => ({
//   getAllUserInfo: () => {
//     dispatch({
//       type: GET_ALL_USER_INFO_REQUEST,
//       payload: {},
//     });
//   },
// });
// const HomeView = ({ id, name, getAllUserInfo, navigation }) => {
//   useEffect(() => {
//     getAllUserInfo();
//   }, [getAllUserInfo]);
//   return (
//     <View>
//       <Text>{this.Maps}</Text>
//     </View>
//   );
// };
// const Home = connect(mapStateToProps, mapDispatchToProps)(HomeView);
// export { Home };
