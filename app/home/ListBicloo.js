import React from "react";
import {
  TouchableHighlight,
  StyleSheet,
  View,
  Image,
  Text,
  Linking,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { connect } from "react-redux";

class ListBicloo extends React.Component {
  state = {
    result: [],
  };

  async componentDidMount() {
    const bicloo = this.props.bicloo;
    const address = bicloo.address;

    const result = {
      address,
    };

    this.setState({ result });
  }
  render() {
    const { result } = this.state;

    return (
      <View>
        <Text>
          {result.address}
          {console.log("result.address", result.address)}
        </Text>
      </View>
    );
  }
}

export default connect()(ListBicloo);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
