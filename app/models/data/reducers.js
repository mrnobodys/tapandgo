import { GET_ALL_USER_INFO_REQUEST_SUCCESS } from "./actions";

const initialState = {
  id: "id1",
  name: "TOTO",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_USER_INFO_REQUEST_SUCCESS: {
      const { id, name } = action.payload;
      return {
        id,
        name,
      };
    }
    default:
      return state;
  }
};

export { reducer };
