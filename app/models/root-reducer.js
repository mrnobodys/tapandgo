import { combineReducers } from "redux";
import { reducer as userReducer } from "./data/reducers";

const reducer = combineReducers({
  user: userReducer,
});

export { reducer };
