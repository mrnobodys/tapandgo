import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { store } from "./store";
import { Provider } from "react-redux";
import { App } from "./app/index";

const Root = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

export default Root;
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "#fff",
//     alignItems: "center",
//     justifyContent: "center",
//   },
// });
